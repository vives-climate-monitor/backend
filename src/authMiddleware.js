module.exports = (req, res, next) => {
    // get the token from the header if present
    const token = req.headers.Authorization || req.headers.authorization
    // if no token found, return response (without going to the next middelware)
    if (!token)
        return res
            .status(401)
            .json({ msg: 'Access denied. No token provided.' })
    if (token === process.env.SECRET_KEY) return next()
    return res.status(400).json({ msg: 'Invalid token.' })
}
