const bodyParser = require('body-parser')
const app = require('express')()
const ajv = require('ajv')()
require('dotenv').config()

app.use(bodyParser.json())

const auth = require('./authMiddleware')
const DB = require('./db')
const metingSchema = require('./metingSchema')

app.get('/', (req, res) => {
    res.json({
        msg: 'Climate Monitor BACKEND ONLINE',
        version: process.env.npm_package_version
    })
})

app.get('/auth', auth, (req, res) => {
    res.json({ msg: 'AUTH OK' })
})

app.post('/meting', auth, async (req, res) => {
    const valid = ajv.validate(metingSchema, req.body)
    if (valid) {
        const result = await DB.saveMeting(req.body)
        return res.json({ meting: result, successfull: true })
    }
    return res.json(ajv.errors)
})

module.exports = app
