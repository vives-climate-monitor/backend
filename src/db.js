const knex = require('./dbconfig')

class Database {
    constructor() {
        console.log('Database helper created!')
    }

    async saveMeting(meting) {
        const id = await knex('metings').insert({
            ...meting
        })

        return this.getMetingById(id)
    }

    async getMetingById(id) {
        return knex('metings').where('id', id)
    }
}

module.exports = new Database()
