module.exports = {
    type: 'object',
    additionalProperties: false,
    properties: {
        co2: { type: 'integer', "maximum": 8192, "minimum": 400 },
        humidity: { type: 'integer', "maximum": 100, "minimum": 0 },
        temperature: { type: 'integer', "maximum": 50, "minimum": 0 },
        tvoc: { type: 'integer', "maximum": 1187, "minimum": 0 },
        room: { type: 'string', "maxLength": 10, "minLength": 0 },
        created_at: { type: 'string', "maxLength": 50, "minLength": 10},
        updated_at: { type: 'string', "maxLength": 50, "minLength": 10},
    },
    required: ['co2', 'humidity', 'temperature', 'tvoc', 'room', 'created_at', 'updated_at']
}
